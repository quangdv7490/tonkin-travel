module.exports = function(grunt) {
  grunt.initConfig({
    sprite:{
      all: {
        src: 'img/icons/*.png',
        retinaSrcFilter: ['img/icons/*@2x.png'],
        dest: 'img/spritesheet.png',
        retinaDest: 'img/spritesheet@2x.png',
        destCss: 'less/spritesheet.css'
      }
    },
    less: {
      development: {
        options: {
          compress: false,
          yuicompress: true,
          optimization: 2
        },
        files: {
          // target.css file: source.less file
          "css/custom/custom-style.css": "less/custom-style.less"
        }
      }
    },
    watch: {
      styles: {
        files: ['less/**/*.less'], // which files to watch
        tasks: ['less'],
        options: {
          nospawn: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.registerTask('default', ['watch']);
};